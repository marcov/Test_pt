# -*- coding: utf-8 -*-

import os
import random

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn
from torch.autograd import Variable

class Generator(nn.Module):
    """Generator """
    def __init__(self, num_emb, emb_dim, hidden_dim, use_cuda):
        super(Generator, self).__init__()
        self.num_emb = num_emb
        self.emb_dim = emb_dim
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.emb = nn.Embedding(num_emb, emb_dim)        
        self.lstm = nn.LSTM(emb_dim, hidden_dim, batch_first=True)
        self.lin = nn.Linear(hidden_dim, num_emb)
        self.softmax = nn.LogSoftmax(dim=1)        
        self.init_params()

    def forward(self, x, length=None):
        """
        Args:
            x: (batch_size, seq_len), sequence of tokens generated by generator
            length: (length), length of the sequence without padding
        """
        emb = self.emb(x)        
        state = self.init_state(x.size(0))

        if length is not None:
            packed = rnn.pack_padded_sequence(emb, length, batch_first=True)
            output, state = self.lstm(packed, state)
            output, _ = rnn.pad_packed_sequence(output, batch_first=True)
        else:
            output, state = self.lstm(emb, state)

        output = self.lin(output.contiguous().view(-1, self.hidden_dim))
        pred = self.softmax(output)        
        return pred

    def step(self, x, state):
        """
        Args:
            x: (batch_size,  1), sequence of tokens generated by generator
            state: (1, batch_size, hidden_dim), lstm state             
        """
        emb = self.emb(x)        
        output, state = self.lstm(emb, state)
        pred = F.softmax(self.lin(output.view(-1, self.hidden_dim)), dim=1)
        return pred, state


    def init_state(self, batch_size):
        h = Variable(torch.zeros((1, batch_size, self.hidden_dim)))
        c = Variable(torch.zeros((1, batch_size, self.hidden_dim)))
        if self.use_cuda:            
            h, c = h.cuda(), c.cuda()
        return (h, c)
    
    def init_params(self):
        for param in self.parameters():                     
            param.data.uniform_(0, 0.1)

    def get_embeddings(self):                
        return self.emb.weight.data

    def sample(self, batch_size, seq_len, x=None):
        res = []
        flag = False # whether sample from zero
        if x is None:
            flag = True
        if flag:
            x = Variable(torch.zeros((batch_size, 1)).long())
        if self.use_cuda:
            x = x.cuda()
        state = self.init_state(batch_size)
        samples = []
        if flag:
            for i in range(seq_len):
                output, state = self.step(x, state)
                x = output.multinomial(1)
                samples.append(x)
        else:
            given_len = x.size(1)
            lis = x.chunk(x.size(1), dim=1)
            for i in range(given_len):
                output, state = self.step(lis[i], state)
                samples.append(lis[i])
            x = output.multinomial(1)
            for i in range(given_len, seq_len):
                samples.append(x)
                output, state = self.step(x, state)
                x = output.multinomial(1)
        output = torch.cat(samples, dim=1)
        return output
