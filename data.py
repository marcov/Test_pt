import os
import torch
import torchtext
import numpy as np
import re

from collections import Counter

class Dictionary(object):
    def __init__(self):
        self.stoi = {}
        self.itos = []
        self.counter = Counter()
        self.embeddings = torch.FloatTensor(0)
        self.total = 0        

    def add_word(self, word):
        if word not in self.stoi:               
            self.stoi[word] = len(self.itos)
            self.itos.append(word)
        token_id = self.stoi[word]
        self.counter[token_id] += 1
        self.total += 1                          
        return self.stoi[word]

    def set_embeddings(self, embeddings):
        self.embeddings = torch.from_numpy(embeddings).float()

    def get_id(self, word):
        return self.stoi.get(word)

    def get_word(self, id):
        return self.itos[id] 

    def __len__(self):
        return len(self.stoi)


class Corpus(object):
    def __init__(self, input_path, vocab_path=None, glove=None):
        if glove is  None:
            self.dictionary = Dictionary()
            self.train = self.tokenize(input_path)
        else:
            self.dictionary = Dictionary()
            embeddings = self.load_embeddings(glove)            
            self.dictionary.set_embeddings(embeddings)
            self.train = self.tokenize_with_vocab(input_path)
        
    def load_embeddings(self, glove):

        glove_d = glove.split('.')[2]
        emb_size = int(re.findall('\d+', glove_d)[0])
        embeddings = np.zeros((400003, emb_size))

        sos = np.random.randn(1, emb_size)            
        index = self.dictionary.add_word('<sos>')            
        embeddings[index] = sos
        eos = np.random.randn(1, emb_size)            
        index = self.dictionary.add_word('<eos>')            
        embeddings[index] = eos
        unk = np.random.randn(1, emb_size)            
        index = self.dictionary.add_word('<unk>')            
        embeddings[index] = unk

        glove_path = '.vector_cache/' + glove + '.txt'
        with open(glove_path, 'r') as f:                
            for line in f.readlines():
                values = line.split()                    
                word = values[0]
                index = self.dictionary.add_word(word)                    
                if index:
                    vector = np.array(values[1:], dtype='float32')
                    embeddings[index] = vector                 
        
        return embeddings

    def tokenize_with_vocab(self, input_path):
        ids = torch.LongTensor(0)
        with open(input_path, 'r') as f:                
            for line in f:
                words = line.split() + ['<eos>']
                for word in words:
                    i = self.dictionary.stoi.get(word)
                    if i is None:
                        i = self.dictionary.stoi.get('<unk>')
                    w = torch.LongTensor([i])
                    ids = torch.cat((ids, w))
        return ids

    def tokenize(self, input_path):
        """Tokenizes a text file."""
        # Add words to the dictionary                
        self.dictionary.add_word('<sos>')

        with open(input_path, 'r') as f:
            tokens = 0
            for line in f:
                words = line.split() + ['<eos>']
                tokens += len(words)
                for word in words:
                    self.dictionary.add_word(word)

        # Tokenize file content
        with open(input_path, 'r') as f:
            ids = torch.LongTensor(tokens)
            token = 0
            for line in f:
                words = line.split() + ['<eos>']
                for word in words:
                    ids[token] = self.dictionary.stoi.get(word)
                    token += 1
        
        return ids