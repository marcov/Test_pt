# -*- coding:utf-8 -*-

import os
import random
import math

import tqdm

import numpy as np
import torch

class GenDataIter(object):
    """ Toy data iter to load digits"""
    def __init__(self, data_file, batch_size, sequence_len):
        super(GenDataIter, self).__init__()
        self.batch_size = batch_size
        self.data_lis = self.read_file(data_file)        
        self.data_num = len(self.data_lis)
        self.indices = range(self.data_num)
        self.num_batches = int(math.ceil(float(self.data_num)/self.batch_size))
        self.idx = 0        
        self.pad_token = 0
        self.sequence_len = sequence_len

    def __len__(self):
        return self.num_batches

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()
    
    def reset(self):
        self.idx = 0
        random.shuffle(self.data_lis)

    def next(self):
        if self.idx >= self.data_num or self.data_num - self.idx < self.batch_size:
            raise StopIteration        
        index = self.indices[self.idx:self.idx+self.batch_size]                          
        length = [min(len(self.data_lis[i])+1, self.sequence_len) for i in index]        
        length, idx_sort = np.sort(length)[::-1], np.argsort(length)[::-1]                
        max_length = max(length) + 1                  
        padded_data = np.ones((self.batch_size, max_length-2)) * self.pad_token             
        padded_target = np.ones((self.batch_size, max_length-1))
        j = 0        
        for i in idx_sort:            
            sequence = self.data_lis[i+self.idx]              
            padded_data[j, 0:length[j]-1] = sequence[:length[j]-1]  
            padded_target[j, 0:length[j]-1] = sequence[:length[j]-1]                
            j += 1     
        padded_data = torch.LongTensor(np.asarray(padded_data, dtype='int64'))                    
        data = torch.cat([torch.zeros(self.batch_size, 1).long(), padded_data], dim=1)   
        target = torch.LongTensor(np.asarray(padded_target, dtype='int64'))                            
        self.idx += self.batch_size         
        return data, target, length

    def read_file(self, data_file):
        with open(data_file, 'r') as f:
            lines = f.readlines()
        lis = []
        for line in lines:
            l = line.strip().split(' ')
            l = [int(s) for s in l]
            lis.append(l)
        return lis    

class DisDataIter(object):
    """ Toy data iter to load digits"""
    def __init__(self, real_data_file, fake_data_file, batch_size, sequence_len):
        super(DisDataIter, self).__init__()
        self.batch_size = batch_size
        self.real_data_lis = self.read_file(real_data_file)
        self.fake_data_lis = self.read_file(fake_data_file)
        self.data = self.real_data_lis + self.fake_data_lis
        self.labels = [1 for _ in range(len(self.real_data_lis))] +\
                        [0 for _ in range(len(self.fake_data_lis))]
        self.pairs = list(zip(self.data, self.labels))
        self.data_num = len(self.pairs)
        self.indices = range(self.data_num)
        self.num_batches = int(math.ceil(float(self.data_num)/self.batch_size))
        self.idx = 0
        self.pad_token = 0
        self.sequence_len = sequence_len

    def __len__(self):
        return self.num_batches

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()
    
    def reset(self):
        self.idx = 0
        random.shuffle(self.pairs)    

    def next(self):
        if self.idx >= self.data_num or self.data_num - self.idx < self.batch_size:
            raise StopIteration
        index = self.indices[self.idx:self.idx+self.batch_size]   
        length = [min(len(self.data[i])+1, self.sequence_len) for i in index] 
        pairs = [self.pairs[i] for i in index]       
        data = [p[0] for p in pairs]
        label = [p[1] for p in pairs]
        padded_data = np.ones((self.batch_size, self.sequence_len)) * self.pad_token
        j = 0
        for i in range(0, self.batch_size):            
            sequence = self.data[i+self.idx]              
            padded_data[j, 0:length[j]-1] = sequence[:length[j]-1]
            j += 1
        data = torch.LongTensor(np.asarray(padded_data, dtype='int64'))
        label = torch.LongTensor(np.asarray(label, dtype='int64'))
        self.idx += self.batch_size
        return data, label, None    

    def read_file(self, data_file):
        with open(data_file, 'r') as f:
            lines = f.readlines()
        lis = []
        for line in lines:
            l = line.strip().split(' ')
            l = [int(s) for s in l]
            lis.append(l)
        return lis
