# -*- coding:utf-8 -*-

import os
import random
import math
import sys

import argparse

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable

from tensorboardX import SummaryWriter

from generator import Generator
from target_lstm import TargetLSTM
from data_iter import GenDataIter, DisDataIter

from data import Corpus

# import dataset
# ================== Parameter Definition =================

parser = argparse.ArgumentParser(description='Training Parameter')
parser.add_argument('--cuda', action='store', default=None, type=int)

# Model parameters.
parser.add_argument('--save', type=str, default='')
parser.add_argument('--resume', type=str, default='')

parser.add_argument('--total_batch', type=int, default=100)
parser.add_argument('--batch_size', type=int, default=20)
parser.add_argument('--generated_seq', type=int, default=100)
parser.add_argument('--vocab_size', type=int, default=6096)

parser.add_argument('--target_data', type=str, default="./coco_image/coco_image.txt")
parser.add_argument('--train_data', type=str, default="train.data")
parser.add_argument('--gene_data', type=str, default="gene.data")
parser.add_argument('--eval_data', type=str, default="eval.data")
parser.add_argument('--sample_data', type=str, default="sample.csv")
parser.add_argument('--embeddings', type=str, default=None)

parser.add_argument('--g_pre_epochs', type=int, default=8)
parser.add_argument('--g_emb_dim', type=int, default=32)
parser.add_argument('--g_hidden_dim', type=int, default=32)
parser.add_argument('--g_sequence_len', type=int, default=20)

opt = parser.parse_args()
print(opt)

# Basic Training Paramters
SEED = 88
BATCH_SIZE = opt.batch_size
TOTAL_BATCH = opt.total_batch
GENERATED_SEQ = opt.generated_seq
SEQUENCE_LEN = opt.g_sequence_len
VOCAB_SIZE = opt.vocab_size

POSITIVE_FILE = opt.train_data
NEGATIVE_FILE = opt.gene_data
EVAL_FILE = opt.eval_data

if opt.cuda is not None and opt.cuda >= 0:
    torch.cuda.set_device(opt.cuda)
    opt.cuda = True

# Genrator Parameters
g_emb_dim = opt.g_emb_dim
g_hidden_dim = opt.g_hidden_dim
g_sequence_len = opt.g_sequence_len

writer = SummaryWriter()

# generate samples 
def generate_samples(model, batch_size, generated_num, output_file):
    samples = []
    for _ in range(int(generated_num / batch_size)):
        sample = model.sample(batch_size, g_sequence_len).data.cpu().numpy().tolist()                                                 
        samples.extend(sample)          
    with open(output_file, 'w') as fout:         
        for sample in samples:              
            string = ' '.join([str(s) for s in sample])
            fout.write('%s\n' % string)         

def generate_eval(corpus):
    i = 0
    eos = corpus.dictionary.get_id('<eos>')
    with open(EVAL_FILE, 'w') as e:
        with open(NEGATIVE_FILE, 'r') as n:
            for line in n.readlines():
                tokens = line.split(' ')
                for token in tokens:                                     
                    if token == eos:
                        break               
                    e.write(corpus.dictionary.get_word(int(token)) + ' ')
                e.write('\n')

def generate_corpus():    
    corpus = Corpus(opt.target_data)    

    with open(POSITIVE_FILE, "w") as f:
        ids = corpus.train
        eos = corpus.dictionary.get_id('<eos>')        
        count = 0
        for id in ids:  
            value = id.data.cpu().numpy()                          
            f.write(str(value) + ' ')
            count += 1
            if value == eos:
                while count < SEQUENCE_LEN:
                    f.write('0 ')
                    count += 1
                count = 0
                f.write('\n')
    return corpus        

def target_loss(target_lstm, data_iter):    
    nll = []
    data_iter.reset()

    lloss = LLoss()    
    for data, _, _ in data_iter:  
        data = Variable(data)
        if opt.cuda:
            data = data.cuda()
        pred = target_lstm(data)
        g_loss = lloss(data, pred) / (BATCH_SIZE * SEQUENCE_LEN)
        nll.append(g_loss.data.cpu().numpy())
    
    return np.mean(nll)

def jsd_calculate(generator, oracle, sample_window=20):
    real_s = []
    fake_s = []
    jsd = []  
    p = Prediction()  
    two = torch.FloatTensor([2.0])    
    for it in range(sample_window):
        real_s.append(oracle.sample(BATCH_SIZE, SEQUENCE_LEN))
        fake_s.append(generator.sample(BATCH_SIZE, SEQUENCE_LEN))        
    for s in real_s:
        if opt.cuda:
            s = s.cuda()
        u_pred = oracle(s)
        v_pred = generator(s)        
        if opt.cuda:
            u_pred = u_pred.cuda()
            v_pred = v_pred.cuda()
        u = p(s, u_pred)
        v = p(s, v_pred)
        log_kl_pm = torch.log(two) - torch.mean(torch.log1p(torch.exp(v - u)))                        
        jsd.append(log_kl_pm.data.numpy().tolist())
    for s in fake_s:
        if opt.cuda:
            s = s.cuda()
        u_pred = oracle(s)
        v_pred = generator(s)
        if opt.cuda:
            u_pred = u_pred.cuda()
            v_pred = v_pred.cuda()
        u = p(s, u_pred)
        v = p(s, v_pred)
        log_kl_gm = torch.log(two) - torch.mean(torch.log1p(torch.exp(u - v)))                       
        jsd.append(log_kl_gm.data.numpy().tolist())                     
    jsd = np.mean(jsd)
    return jsd

class Prediction(nn.Module):
    def __init__(self):
        super(Prediction, self).__init__()

    def forward(self, x, pred):        
        N = x.size()[0] * x.size()[1]        
        one_hot = torch.zeros(N, VOCAB_SIZE)
        if x.is_cuda:
            one_hot = one_hot.cuda()
        one_hot.scatter_(1, x.data.contiguous().view(-1, 1), 1)        
        one_hot = one_hot.type(torch.ByteTensor)
        one_hot = Variable(one_hot)            
        if x.is_cuda:
            one_hot = one_hot.cuda()                          
        pred = torch.masked_select(pred, one_hot)                
        pred = pred.data.cpu().contiguous().view(-1, SEQUENCE_LEN)                        
        pred = torch.sum(pred, dim=0)  
        pred = torch.sum(pred, dim=0)          
        return pred

class LLoss(nn.Module):
    """Reward-Refined NLLLoss Function for adversial training of Gnerator"""
    def __init__(self):
        super(LLoss, self).__init__()

    def forward(self, x, pred):
        """
        Args:
            prob: (N, C), torch Variable 
            pred : (N*C, V), torch Variable            
        """
        N = pred.size(0)
        one_hot = torch.zeros((N, VOCAB_SIZE)) # (BATCH_SIZE*SEQUENCE_LEN, VOCAB_SIZE)  
        if x.is_cuda:
            one_hot = one_hot.cuda()                
        # print(x.contiguous().view(-1,1).size())
        # print(one_hot.size())
        one_hot.scatter_(1, x.contiguous().view(-1,1), 1)        
        one_hot = one_hot.type(torch.ByteTensor)
        one_hot = Variable(one_hot)
        if x.is_cuda:
            one_hot = one_hot.cuda()                  
        loss = torch.masked_select(pred, one_hot)                
        loss = -torch.sum(loss)
        return loss

def main():         
    random.seed(SEED)
    np.random.seed(SEED)

    global writer
    
    corpus = generate_corpus()
    VOCAB_SIZE = len(corpus.dictionary)     

    # Define Networks
    generator = Generator(VOCAB_SIZE, g_emb_dim, g_hidden_dim, opt.cuda)
    gen_criterion = nn.NLLLoss(ignore_index=0)
    gen_optimizer = optim.Adam(generator.parameters())    
    
    mediator = Generator(VOCAB_SIZE, g_emb_dim*2, g_hidden_dim*2, opt.cuda)
    med_criterion = nn.NLLLoss(ignore_index=0)
    med_optimizer = optim.Adam(mediator.parameters())

    # target_lstm
    # target_lstm = TargetLSTM(VOCAB_SIZE, g_emb_dim, g_hidden_dim, opt.cuda)    

    if opt.cuda:
        generator = generator.cuda()
        gen_criterion = gen_criterion.cuda()
        mediator = mediator.cuda()
        med_criterion = med_criterion.cuda()
        # target_lstm = target_lstm.cuda()    

    # data loader
    # generate_samples(target_lstm, BATCH_SIZE, GENERATED_SEQ, POSITIVE_FILE)
    gen_data_iter = GenDataIter(POSITIVE_FILE, BATCH_SIZE, SEQUENCE_LEN)    
    # generate_samples(target_lstm, BATCH_SIZE, GENERATED_SEQ, EVAL_FILE)    
    # val_data_iter = GenDataIter(EVAL_FILE, BATCH_SIZE, SEQUENCE_LEN)    

    # Pre-train generator

    # Cooperative Training     
    print('#####################################################')
    print('Start Cooperative Training...')
    lloss = LLoss()    
    for iteration in range(TOTAL_BATCH):        
        # Train the generator
        for it in range(2):
            samples = generator.sample(BATCH_SIZE, SEQUENCE_LEN)                               
            rewards = mediator(torch.cat((samples, samples), dim=0))            
            pred = generator(samples)            
            rewards = rewards[0: BATCH_SIZE*SEQUENCE_LEN]
            loss = -torch.sum(torch.exp(pred) * (rewards - pred)) / BATCH_SIZE                                                                 
            gen_optimizer.zero_grad()
            loss.backward()
            gen_optimizer.step()            
        writer.add_scalar('data/gen_loss', loss, global_step=(iteration))
        #Test (compare the generated text wrt oracle)
        # if iteration % 10 == 0 or iteration == TOTAL_BATCH - 1:
        #     generate_samples(generator, BATCH_SIZE, GENERATED_SEQ, NEGATIVE_FILE)
        #     likelihood_data_iter = GenDataIter(NEGATIVE_FILE, BATCH_SIZE, SEQUENCE_LEN)
        #     test_loss = target_loss(target_lstm, likelihood_data_iter)         
                        
        #     buffer = 'batch:' + str(iteration) + '\tnll_oracle:\t' + str(test_loss) + '\n'
        #     print('batch: ', iteration, 'nll_oracle: ', test_loss)             
        # # compare the oracle text wrt generator
        # if iteration % 10 == 0 or iteration == TOTAL_BATCH - 1:
        #     test_loss = target_loss(generator, val_data_iter)
        #     print('batch:', iteration, 'nll_test ', str(test_loss))
        #     writer.add_scalar('data/nll_test', test_loss, global_step=iteration)

        # Train the mediator
        for _ in range(1): 
            bnll_ = []
            collected_x = []
            length_x = []
            ratio = 2
            for it in range(ratio):
                if it % 2 == 0:
                    _, x_batch, length = next(gen_data_iter)    # real data                 
                else:
                    x_batch = generator.sample(BATCH_SIZE, SEQUENCE_LEN)  # generated data
                    length = np.full(BATCH_SIZE, SEQUENCE_LEN)
                collected_x.append(x_batch.data.cpu().numpy())
                length_x.append(length)
                        
            collected_x = np.reshape(collected_x, [-1, SEQUENCE_LEN])            
            length_x = np.reshape(length_x, [BATCH_SIZE*2])            
            idx = np.random.permutation(len(length_x))
            collected_x, length_x = collected_x[idx], length_x[idx] 
            # np.random.shuffle(collected_x)               
            collected_x = np.reshape(collected_x, [-1, BATCH_SIZE*2, SEQUENCE_LEN])
            length_x = np.reshape(length_x, [-1, BATCH_SIZE*2])
            collected_x = torch.from_numpy(collected_x)

            for it in range(1):
                temp = collected_x[it]                     
                length = length_x[it]                
                length, idx_sort = np.sort(length)[::-1], np.argsort(length)[::-1]
                j = 0
                x = []              
                for i in idx_sort:
                    x = np.concatenate((x, temp[i]), axis=0)
                    j += 1 
                x = x.astype(int)
                x = np.reshape(x, [-1, SEQUENCE_LEN])
                x = torch.from_numpy(x)
                if opt.cuda:
                    x = x.cuda()
                pred = mediator(x, length)                   
                bnll = lloss(x, pred) / (BATCH_SIZE * 2 * SEQUENCE_LEN)
                bnll_.append(bnll.data.cpu().numpy())                
                med_optimizer.zero_grad()
                bnll.backward()
                med_optimizer.step()
                        
        if (iteration * 4) % len(gen_data_iter) == 0:
            bnll = np.mean(bnll_)
            sample1 = generator.sample(BATCH_SIZE, SEQUENCE_LEN)
            sample2 = generator.sample(BATCH_SIZE, SEQUENCE_LEN)     
            x = torch.cat((sample1, sample2), dim=0)
            pred = mediator(x)
            gnll = lloss(x, pred) / (BATCH_SIZE * 2 * SEQUENCE_LEN)                 
            print("mediator cooptrain iter#%d, balanced_nll %f, g_nll %f" % (iteration, bnll, gnll))
            writer.add_scalar('data/bnll', bnll, global_step=iteration)
            writer.add_scalar('data/gnll', gnll, global_step=iteration)

            generate_samples(generator, BATCH_SIZE, GENERATED_SEQ, NEGATIVE_FILE)
            generate_eval(corpus)
            print('=== EVAL.DATA sample (first 10 rows) ===')
            os.system('head -n 10 eval.data') 
            print(' ')     
        # if iteration % len(gen_data_iter) == 0:
        #     jsd = jsd_calculate(generator, target_lstm)
        #     print('cooptrain epoch#', iteration // len(gen_data_iter), 'jsd ', jsd)
        #     writer.add_scalar('data/jsd', jsd)

        gen_data_iter.reset()

if __name__ == '__main__':
    main()
